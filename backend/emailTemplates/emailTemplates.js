const styles = require("./styles");
// const logo = require("../assets/logo.png");

const toCustomer = order => {
  let orderComponent = order.cart
    .map(
      el => `
    <div style=${styles.cartItem}>
    <div style=${styles.imageContainer}>
    <img src=${el.image} alt="product" style=${styles.image} width="100%" height="100%"/>
    </div>
        <div style=${styles.textContainer}>
            <h3 style=${styles.texts}>Product: ${el.name}</h3>
            <h3 style=${styles.texts}>Price: ${el.price} €</h3>
            <h3 style=${styles.texts}>Quantity: ${el.quantity}</h3>
        </div>
    </div>
`
    )
    .join("");

  return `<div style=${styles.container}>
  <img src="https://res.cloudinary.com/dom04nwee/image/upload/v1570109419/fgotolg7id8qjha9cbur.png" alt="logo" width="100px" height="100px"/>
    <h1 style=${styles.texts}>
        Order Confirmation #${order._id}
    </h1>
    <h2 style=${styles.texts}>Hello ${order.username}!</h2> 
    <p style=${styles.texts}>Thanks for your order. Your order will be arriving in an hour.</p>
    <h3 style=${styles.texts}>Your order details</h3>
        <p style=${styles.texts}>Ordered on: ${order.date}</p>
        ${orderComponent}
       <h1 style=${styles.texts}>Total:  ${order.total} €</h1>
</div>`;
};

const toAdmin = order => {
  let orderComponent = order.cart
    .map(
      el => `
    <div style=${styles.cartItem}>
    <div style=${styles.imageContainer}>
    <img src=${el.image} alt="product" width="100%" height="100%"/>
    </div>
        <div style=${styles.textContainer}>
            <div  style=${styles.textElement}><p>Product: ${el.name}</p></div>
            <div  style=${styles.textElement}><p>Price: ${el.price} €</p></div>
            <div  style=${styles.textElement}><p>Quantity: ${el.quantity} €</p></div>
        </div>
    </div>
`
    )
    .join("");

  return `<div style=${styles.container}>
    <h1>
        Order Confirmation #${order._id}
    </h1>
    <h2>Hello admin!</h2> 
    <p>${order.username} has ordered these on ${order.date}: </p>
        ${orderComponent}
        <h1>Total: ${order.total} €</h1>
</div>`;
};

// module.exports = (name,order_id=1542) => (
//     `<div  style=${styles.container}>
//          <h1>Hello ${name}, thankyou for your order!</h1>
//          <h3>Your order number is ${order_id}</h3>
//            ${
//              [
//              {
//                img_link:'https://i5.walmartimages.com/asr/209bb8a0-30ab-46be-b38d-58c2feb93e4a_1.1a15fb5bcbecbadd4a45822a11bf6257.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF',
//                name:'banana',
//                price:375,
//                quantity:3
//              },{img_link:'https://www.comenaranjas.com/images/stories/virtuemart/product/kiwi-jumbo.jpg',
//                 name:'kiwi',
//                 price:840,
//                 quantity:2
//              }
//              ].map( ele => {
//                 total += ( ele.quantity * ele.price )
//                 return`<div style=${styles.cartItem}>
//                            <div style=${styles.imageContainer}>
//                               <img width="100%" height="100%" src=${ele.img_link} alt=${ele.name}>
//                            </div>
//                            <div style=${styles.textContainer}>
//                               <div  style=${styles.textElement}><p>product: ${ele.name}</p></div>
//                               <div  style=${styles.textElement}><p>price: ${ele.price} €</p></div>
//                               <div  style=${styles.textElement}><p>quantity: ${ele.quantity}</p></div>
//                            </div>
//                        </div>`
//              })
//            }
//            <div style=${styles.footer}>
//                  <p>total ${total} €</p>
//            </div>
//     </div>`
// )

module.exports = {
  toCustomer,
  toAdmin
};
