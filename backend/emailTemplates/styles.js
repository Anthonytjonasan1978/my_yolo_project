module.exports = {
    container : "width:100%;height:100%;font-family:'Arial';",
    cartItem : "height:20%;width:50vw;display:flex;justify-content:space-between;align-items:center",
    imageContainer : "width:30%;height:30%",
    texts: "margin:0;font-size:1.5em;padding:0;",
    textContainer : "width:70%;padding-left:2em",
    image: "object-fit:cover;width:100%;height:100%"
  }