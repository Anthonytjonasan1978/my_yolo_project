const User = require("../models/users");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {secret} = require("../config");
const saltRounds = 10;

const register = async (req, res) => {
  const {
    email,
    username,
    password,
    password2,
    isSellers,
    isAdmin,
    userDescription,
    address
  } = req.body;
  console.log(req.body);
  if (!email || !username || !password || !password2)
    return res.json({ ok: false, message: "All fields are required." });
  if (password !== password2)
    return res.json({ ok: false, message: "Passwords must match." });
  try {
    const user = await User.findOne({ email });
    if (user) return res.json({ ok: false, message: "This email already exists." });
    // const name = await User.findOne({ username: name });
    // if (name) return res.json({ ok: false, message: 'This username already exists. Please choose another one'})
    const hash = await bcrypt.hash(password, saltRounds);
    const newUser = {
      email,
      username,
      password: hash,
      isSellers,
      isAdmin,
      userDescription,
      address
    };
    await User.create(newUser);
    res.json({ ok: true, message: "Successful register" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const login = async (req, res) => {
  const { username, password, isAdmin, isSellers } = req.body;
  if (!username || !password)
    return res.json({ ok: false, message: "All fields are required." });
  try {
    const user = await User.findOne({ username });
    if (!user)
      return res.json({ ok: false, message: "Please provide a valid username." });
    if (isAdmin !== user.isAdmin || isSellers !== user.isSellers) {
        return res.json({ ok: false, message: "You're trying to sign in as the wrong user. You're either a seller, a buyer or an admin."})
    }
    const match = await bcrypt.compare(password, user.password);
    if (match) {
      const token = jwt.sign(user.toJSON(), secret.secret, {
        expiresIn: 100080
      });
      return res.json({
        ok: true,
        message: "welcome back",
        token,
        username,
        isAdmin: user.isAdmin,
        isSellers: user.isSellers,
        userDescription: user.userDescription,
        address: user.address
      });
    } else return res.json({ ok: false, message: "Invalid password." });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const verify_token = (req, res) => {
  const { token } = req.body;
  jwt.verify(token, secret.secret, (err, succ) => {
    err
      ? res.json({ ok: false, message: "Something went wrong." })
      : res.json({
          ok: true,
          message: "secret page",
          isAdmin: succ.isAdmin,
          isSellers: succ.isSellers,
          username: succ.username,
          address: succ.address
        });
  });
};

const changePassword = async (req, res) => {
  const {username, oldPassword, newPassword, newPassword2} = req.body;
  console.log(req.body)
  if (newPassword !== newPassword2) 
    return res.json({ ok: false, message: "Your new passwords don't match. Try again." });
try {
  const user = await User.findOne({ username });
  // const hashOld = await bcrypt.hash(oldPassword, saltRounds)
  const hashNew = await bcrypt.hash(newPassword, saltRounds);
  const matchOld = await bcrypt.compare(oldPassword, user.password);

  if (!matchOld) {
    return res.json({ ok: false, message: "Your old password is incorrect." });
  } else {
    await User.findOneAndUpdate({username}, {$set: {password: hashNew}})
    res.json({ ok: true, message: 'Change successful'})
  }
}catch(err){console.log(err)}}

const updateSellersDescription = async (req, res) => {
  const { username, userDescription } = req.body;
  try {
    const response = await User.findOneAndUpdate(
      { username },
      { $set: { userDescription } }
    );
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const updateAddress = async (req, res) => {
  const { username, address } = req.body;
  try {
    const response = await User.findOneAndUpdate(
      { username },
      { $set: { address } }
    );
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const updateEmail = async (req, res) => {
  const { username, newEmail } = req.body;
  try {
    const response = await User.findOneAndUpdate(
      { username },
      { $set: { email: newEmail } }
    );
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};


const findOneUser = async (req, res) => {
  const { username } = req.query;
  try {
    const response = await User.findOne({ username });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

module.exports = {
  register,
  login,
  verify_token,
  updateSellersDescription,
  findOneUser,
  updateAddress,
  updateEmail,
  changePassword
};
