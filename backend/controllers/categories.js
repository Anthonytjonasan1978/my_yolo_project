const Categories = require("../models/categories");

const add = async (req, res) => {
  const { category } = req.body;
  try {
    const response = await Categories.create({ category });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const remove = async (req, res) => {
  const { category } = req.body;
  try {
    const response = await Categories.deleteOne({ category });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const update = async (req, res) => {
  const { category, newCategory } = req.body;
  try {
    const response = await Categories.update({ category }, { $set: { category: newCategory } });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

// GET ROUTES
const findOne = async (req, res) => {
  const { _id } = req.body;
  try {
    const response = await Categories.findOne({ _id });
    res.send(response.category);
  } catch (err) {
    res.send(err.message);
  }
};

const findAll = async (req, res) => {
  try {
    const response = await Categories.find({});
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

module.exports = {
  add,
  remove,
  update,
  findOne,
  findAll
};
