const nodemailer = require('nodemailer')
// Import pwd form form mailer where we can hide our mailbox password
const { pwd }    = require('../modules/pwd')
const {toCustomer, toAdmin} = require('../emailTemplates/emailTemplates');
const resetPassword = require ('../helpers/resetPassword');

// selecting mail service and authorazing with our credentials
const transport = nodemailer.createTransport({
// you need to enable the less secure option on your gmail account
// https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
	auth: {
		user: `anthony@barcelonacodeschool.com`,
		pass: pwd,
	}
});

const sendEmail = async (req,res) => {
	  const { name , email, order} = req.body
	  const mailOptions = {
		    to: email,
		    subject: `YOLO CO. (Your order confirmation ${name})`,
		    html: toCustomer(order)
       }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({ok:true,message:'email sent'})
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

const sendMessage = async (req, res) => {
	const {fullName, email, message} = req.body
	const mailOptions = {
		to: 'anthony@barcelonacodeschool.com',
		subject: `A new message from ${fullName}`,
		html: `<h1>A new message from this email ${email} </h1>
		</br/> <p>${message}</p>`
	}
	try {
		const response = await transport.sendMail(mailOptions)
		return res.json({ok:true, message: 'email sent'})
	}catch(err) {
		return res.json({ok:false, message:err})
	}
}

const resetPasswordMail = async (req, res) => {
	const {email, newPassword, newPassword2} = req.body;
	const mailOptions = {
		to: email,
		subject: `Your password recovery for SOUL FOOD CO.`,
		html: `<h1>Your password has been resetted.</h1>`
	}
	try {
		const mail = await transport.sendMail(mailOptions)
		resetPassword(res, email, mail, newPassword, newPassword2)
	} catch(err) {
		res.send(err.message)
	}
}


module.exports = { sendEmail, sendMessage, resetPasswordMail }