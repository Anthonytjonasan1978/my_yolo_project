const Products = require("../models/products");

const add = async (req, res) => {
  console.log('hello from res.body //////////',res.body)
  const {
    name,
    price,
    description,
    category,
    stock,
    image
    
  } = req.body;
  try {
    const response = await Products.create({
      name,
      price,
      description,
      category,
      stock,
      image
    
    });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const remove = async (req, res) => {
  const { _id } = req.body;
  try {
    const response = await Products.deleteOne({ _id });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const removeMany = async (req, res) => {
  const { category } = req.body;
  try {
    const response = await Products.deleteMany({ category });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const update = async (req, res) => {
  const { _id, updated } = req.body;
  try {
    const response = await Products.updateOne(
      { _id },
      {
        $set: updated
      }
    );
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const updateCategory = async (req, res) => {
  const { category, newCategory } = req.body;
  try {
    const response = await Products.updateMany(
      { category },
      { category: newCategory }
    );
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

// GET REQUESTS
const findAllProducts = async (req, res) => {
  try {
    const response = await Products.find({});
    console.log(response,'===========')
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const findOneProduct = async (req, res) => {
  const { _id } = req.query;
  try {
    const response = await Products.findOne({ _id });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const findProductsOfCategory = async (req, res) => {
  const { category } = req.query;
  try {
    const response = await Products.find({ category });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const findProductsOfSellers = async (req, res) => {
  const { Sellers } = req.query;
  try {
    const response = await Products.find({ sellers });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const findStorageArray = async (req, res) => {
  const { cart } = req.body;
    let id = [];
    cart.forEach(el => {
      id.push(el.id);
    });
  try {
    const response = await Products.find({ _id: { $in: id } });
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const updateStock = async (req, res) => {
  const { fullCart } = req.body;
  let temp = [];
  fullCart.forEach(el => {
    temp.push({id: el.id, quantity: el.quantity});
  });
  try {
    const response = await Products.findOneAndUpdate(
      { _id: temp.id },
      { stock: stock - temp.quantity}
    );
    res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

module.exports = {
  add,
  remove,
  update,
  findAllProducts,
  findOneProduct,
  updateCategory,
  findProductsOfCategory,
  removeMany,
  findProductsOfSellers,
  findStorageArray,
  updateStock
};
