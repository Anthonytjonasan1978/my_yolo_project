const Orders = require("../models/orders");
const updateStock = require('../helpers/updateStock');
const {time, fullDate} = require('../date/date');

const addOrders = async (req, res) => {
  const { cart, username, total } = req.body;
  try {
    // let date = fullDate;
    let order = await Orders.create({cart, username, total, date: fullDate, time});
    updateStock(res, cart, order)
    //res.send(response);
  } catch (err) {
    res.send(err.message);
  }
};

const getOrdersByUsername = async (req, res) => {
  const { username } = req.params;
  try {
    const response = await Orders.find({username});
    res.send(response)
  } catch(err) {
    res.send(err.message)
  }
}



const getOrdersOfSellers = async (req, res) => {
  const { foodId } = req.body;
  console.log(foodId)
  try {
    const response = await Orders.find({ "cart.id":  {$in: foodId} })
    res.send(response)
  } catch(err) {
    res.send(err)
  }
}


const getAllOrders = async (req, res) => {
  try {
    const response = await Orders.find({});
    res.send(response)
  } catch(err) {
    res.send(err.message)
  }
}

const sortOrdersByDate = async (req, res) => {
  try {
    Orders.find({}).sort('-date').exec(function(err, docs) { 
      res.send(docs)
     });
  } catch(err) {
    res.send(err.message)
  }
}

module.exports = {
  addOrders,
  getOrdersByUsername,
  getAllOrders,
  sortOrdersByDate,
  getOrdersOfSellers
};
