const SellersSales = require("../models/sellersSales");
const {time, fullDate} = require('../date/date');

const createSellersOrders = async (req, res) => {
    const { challengeId, name, quantity, price, username} = req.body;
    try {
        const response = await sellersSales.create({
            challengeId,
            name,
            quantity,
            price,
            username,
            date: fullDate,
            time
        })
        res.send(response)
    } catch(err) {
        res.send(err.message)
    }
}

const findOrdersOfSellers = async (req, res) => {
    const {challengeIds} = req.body;
    try {
        const response = await sellersSales.find({ challengeId : { $in: challengeIds }})
        res.send(response)
    } catch(err) {
        res.send(err.message)
    }
}

const sortSellersOrdersByDate = async (req, res) => {
    const {challengeIds} = req.body;
    try {
        //figure out how to find and sort or sort it in react
      const response = await sellersSales.find({ challengeId : { $in: challengeIds }}).sort({date: -1})
        res.send(response)
       ;
    } catch(err) {
      res.send(err.message)
    }
  }

module.exports = {
    createSellersOrders,
    findOrdersOfSellers,
    sortSellersOrdersByDate
}