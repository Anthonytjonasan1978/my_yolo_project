const router = require('express').Router();
const controller = require('../controllers/users')

router.post('/register',controller.register);
router.post('/login',controller.login);
router.post('/verify_token',controller.verify_token);
router.post('/updateSellersDescription', controller.updateSellersDescription)
router.post('/updateAddress', controller.updateAddress)
router.post('/updateEmail', controller.updateEmail)
router.post('/changePassword', controller.changePassword)

router.get('/findOneUser', controller.findOneUser)

module.exports = router;