const router = require('express').Router();
const controller = require('../controllers/categories')

router.post('/add', controller.add)
router.post('/remove', controller.remove)
router.post('/update', controller.update)

router.get('/findOne', controller.findOne)
router.get('/findAll', controller.findAll)

module.exports = router;