const router     = require('express').Router();
const controller = require('../controllers/sellersSales')

router.post('/createSellersOrders',controller.createSellersOrders)
router.post('/findOrdersOfSellers', controller.findOrdersOfSellers)
router.post('/sortSellersOrdersByDate', controller.sortSellersOrdersByDate)

module.exports = router