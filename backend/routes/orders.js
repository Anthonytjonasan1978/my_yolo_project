const router = require('express').Router();
const controller = require('../controllers/orders')

router.post('/addOrders', controller.addOrders)

router.get('/getAllOrders', controller.getAllOrders)
router.get('/getOrdersByUsername/:username', controller.getOrdersByUsername)
router.get('/sortOrdersByDate', controller.sortOrdersByDate);
router.post('/getOrdersOfSellers', controller.getOrdersOfSellers)


module.exports = router;