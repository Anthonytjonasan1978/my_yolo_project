const router = require("express").Router();
const controller = require("../controllers/payment");

router.post("/charge", controller.charge);

module.exports = router;
