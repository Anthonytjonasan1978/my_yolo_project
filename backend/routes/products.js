const router = require('express').Router();
const controller = require('../controllers/products')

router.post('/add', controller.add)
router.post('/remove', controller.remove)
router.post('/update', controller.update)
router.post('/updateCategory', controller.updateCategory)
router.post('/removeMany', controller.removeMany)
router.post('/findStorageArray', controller.findStorageArray)
router.post('/updateStock', controller.updateStock)

router.get('/findAllProducts', controller.findAllProducts)
router.get('/findOneProduct', controller.findOneProduct)
router.get('/findProductsOfCategory', controller.findProductsOfCategory)
router.get('/findProductsOfSellers', controller.findProductsOfSellers)


module.exports = router;