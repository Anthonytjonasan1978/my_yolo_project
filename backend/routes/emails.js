const router     = require('express').Router();
const controller = require('../controllers/emails')

router.post('/sendEmail',controller.sendEmail)
router.post('/sendMessage', controller.sendMessage)
router.post('/resetPasswordMail', controller.resetPasswordMail)

module.exports = router