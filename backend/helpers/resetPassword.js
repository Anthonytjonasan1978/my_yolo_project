const User = require("../models/users");
const bcrypt = require("bcrypt");
// const jwt = require("jsonwebtoken");
// const {secret} = require("../config");
const saltRounds = 10;

const resetPassword = async (res, email, mail, newPassword, newPassword2) => {
  if (newPassword !== newPassword2)
    return res.json({
      ok: false,
      message: "Your new passwords don't match. Try again."
    });
  try {
    const hashNew = await bcrypt.hash(newPassword, saltRounds);
    await User.findOneAndUpdate({ email }, { $set: { password: hashNew } });
    res.send({mail});
    // res.json({ ok: true, message: "Change successful" });
  } catch (err) {
    res.json({ ok: false, message: err });
  }
};

//     const {username, oldPassword, newPassword, newPassword2} = req.body;
//     console.log(req.body)
//     if (newPassword !== newPassword2)
//       return res.json({ ok: false, message: "Your new passwords don't match. Try again." });
//   try {
//     const user = await User.findOne({ username });
//     // const hashOld = await bcrypt.hash(oldPassword, saltRounds)
//     const hashNew = await bcrypt.hash(newPassword, saltRounds);
//     const matchOld = await bcrypt.compare(oldPassword, user.password);

//     if (!matchOld) {
//       return res.json({ ok: false, message: "Your old password is incorrect." });
//     } else {
//       await User.findOneAndUpdate({username}, {$set: {password: hashNew}})
//       res.json({ ok: true, message: 'Change successful'})
//     }
//   }catch(err){console.log(err)}}

module.exports = resetPassword;
