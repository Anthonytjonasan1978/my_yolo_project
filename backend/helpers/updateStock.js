const Products = require('../models/products');

const updateStock = async (res, cart, order) => {
    try {
        cart.forEach(async el => {
            let product = await Products.findOne({ _id: el.id })
            await Products.updateOne({_id: el.id}, {stock: product.stock - el.quantity})
        })
        res.send({order})
    } catch(err) {
        res.send(err)
    }
}

module.exports = updateStock