const express = require("express"),
   router = require('express-promise-router'),
  app = express(),
  mongoose = require("mongoose"),
  bodyParser = require("body-parser"),
  cors = require("cors");
cloudinary = require("cloudinary"),
{cloudinaryConfig} = require('./config');

// =================== initial settings ===================
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

//or enable it only for the specific url
app.options("/sendEmail", cors());

// allowing requests from the front-end to our server with api calls
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

  next();
});
//===============================================================================================
//===============================================================================================
//===============================================================================================

// connecting to mongo and checking if DB is running
async function connecting() {
  try {
    await mongoose.connect("mongodb://127.0.0.1/e-commerce-db", {
      useUnifiedTopology: true,
      useNewUrlParser: true
    });
    console.log("Connected to the DB");
  } catch (error) {
    console.log(
      "ERROR: Seems like your DB is not running, please start it up !!!"
    );
  }
}
connecting();
// end of connecting to mongo and checking if DB is running

// routes
app.use("/categories", require("./routes/categories"));
app.use("/products", require("./routes/products"));
app.use("/users", require("./routes/users"));
app.use("/payment", require("./routes/payment"));
app.use("/orders", require("./routes/orders"));
app.use("/sellersSales", require("./routes/sellersSales"));
app.use("/emails", require("./routes/emails"));
app.post("/upload", (req, res) => res.send(req.body));
// Set the server to listen on port 8000
app.listen(8888, () => console.log(`listening on port 8000`));
