const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrdersSchema = new mongoose.Schema ({
    cart: {
        type: Array,
        required: true
    },
    total: {
        type: Number,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('orders', OrdersSchema)