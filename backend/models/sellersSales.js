const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SellersSalesSchema = new mongoose.Schema ({
   challengeId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('sellerssales', SellersSalesSchema)