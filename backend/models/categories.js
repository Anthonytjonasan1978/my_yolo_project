const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategoriesSchema = new mongoose.Schema({
    category: {
        type: String,
        required: true,
        unique: true
    }
})

module.exports = mongoose.model('categories', CategoriesSchema)