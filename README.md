# YOLO  (full stack React website)

## Demo
http://104.248.95.72/

## What is it
This is a final class project that's done in 2 weeks. 
<br>It is a marketplace website where you can register or log in as a seller or a buyer.

## Screenshot
![Screenshot](https://res.cloudinary.com/dkh74tuj6/image/upload/v1580734704/yolo_q28d7y.png)

## Libraries used
<ul>
<li>React.js</li>
<li>Axios</li>
<li>Express</li>
<li>Mongoose</li>
<li>Cloudinary</li>
<li>Bcrypt</li>
<li>BodyParser</li>
<li>Cors</li>
<li>JSONWebToken</li>
<li>Nodemailer</li>
<li>Stripe</li>
</ul>

## Features
<ul>
<li>As a seller of awesome experiences, you can add, remove or update your own product online. You can also see order history and sales, personalize your dashboard, recover your password, change your email.
<li>As a buyer, you can buy products and have them customixed to you. You can also see your order history, recover your password, change your email.
<li>As an admin, you can perform CRUD operations on categories and products. You can see all order histories.
**if you want to try as an admin, please login as "admin" as the username and "12" as the password.
</ul>

## Installing
<ol>
<li>Clone this project</li>
<li>run **npm install** to install all packages needed</li>
<li>For the client, cd e-commerce-frontend and run **npm start**.</li>
<li>For the server, cd server and run **nodemon index**</li>
</ol>


## Versions
1.0 Core features released 

# Developer
Tony Tjon a San (anthonytjonasan1978@gmail.com)
