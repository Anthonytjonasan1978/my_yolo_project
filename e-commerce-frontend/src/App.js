import React, { Component } from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import axios from "axios";
import { URL } from "./config";

import "./App.css";
import Navbar from "./Components/Navbar/Navbar";
import Home from "./Containers/Home/Home";
import Auth from "./Containers/Auth/Auth";
import Menu from "./Components/Menu/Menu";
import Admin from "./Containers/Admin/Admin";
import SettingsProd from "./Components/Settings/SettingsProd";
import SettingsCat from "./Components/Settings/SettingsCat";
import SellersDashboard from "./Components/Dashboards/SellersDashboard";
import UserDashboard from "./Components/Dashboards/UserDashboard";
import Checkout from "./Components/Menu/Checkout";
import AccessError from "./Components/Error/AccessError";
import Orders from "./Components/Orders/Orders";

class App extends Component {
  state = {
    isLoggedin: false,
    isSellers: false,
    isAdmin: false,
    username: "",
    cart: []
  };

  cartFunction = () => {
    let cart = JSON.parse(localStorage.getItem("cart"));
    axios
      .post(`${URL}/products/findStorageArray`, { cart })
      .then(res => {
        let temp = [];
          res.data.forEach(el => {
            let index = cart.findIndex(item => item.id === el._id);
            temp.push({ ...cart[index], price: el.price, image: el.image });
          });
          this.setState({ cart: temp });
        }
      )
      .catch(err => console.log(err));
  };

  verifyToken = () => {
    let token = JSON.parse(localStorage.getItem("token"));
    if (token) {
      axios
        .post(`${URL}/users/verify_token`, {
          token
        })
        .then(res => {
          return res.data.ok
            ? this.setState({
                isLoggedin: true,
                isSellers: res.data.isSellers,
                isAdmin: res.data.isAdmin,
                username: res.data.username
              })
            : console.log("not ok");
        })
        .catch(err => console.log(err));
    }
  };

  login = (token, isSellers, isAdmin, username) => {
    localStorage.setItem("token", JSON.stringify(token));
    this.setState({ isLoggedin: true, isSellers, isAdmin, username });
  };

  logout = () => {
    localStorage.removeItem("token");
    this.setState({ isLoggedin: false, isSellers: false, isAdmin: false });
  };

  componentDidMount() {
    this.verifyToken();
  }

  render() {
    return (
      <>
        <Router>
          <Navbar
            className="Navbar"
            logout={this.logout}
            isLoggedin={this.state.isLoggedin}
            isSellers={this.state.isSellers}
            isAdmin={this.state.isAdmin}
            cart={this.state.cart}
            username={this.state.username}
          />
          <Route exact path="/" component={Home} />
          <Route
            exact
            path="/register/:userState"
            render={props =>
              this.state.isLoggedin ? (
                props.match.params.userState === "admin" &&
                this.state.isAdmin ? (
                  <Redirect to="/admin" />
                ) : props.match.params.userState === "sellers" &&
                  this.state.isSellers ? (
                  <Redirect to={`/sellers/${this.state.username}`} />
                ) : (
                  <Redirect to="/menu" />
                )
              ) : (
                <Auth
                  {...props}
                  login={this.login}
                  isSellers={this.state.isSellers}
                  isAdmin={this.state.isAdmin}
                />
              )
            }
          />
          <Route
            exact
            path="/menu"
            render={props => (
              <Menu
                {...props}
                cartFunction={this.cartFunction}
                cart={this.state.cart}
              />
            )}
          />
          <Route
            exact
            path="/admin"
            render={props =>
              this.state.isAdmin ? (
                <Admin
                  {...props}
                  isAdmin={this.state.isAdmin}
                  username={this.state.username}
                />
              ) : (
                <AccessError
                  state={"admin"}
                  isLoggedin={this.state.isLoggedin}
                />
              )
            }
          />
          <Route
            path="/settings/products/:id"
            render={props =>
              this.state.isAdmin || this.state.isSellers ? (
                <SettingsProd {...props} />
              ) : (
                <AccessError
                  state={"sellers"}
                  isLoggedin={this.state.isLoggedin}
                />
              )
            }
          />

          <Route
            path="/admin/settings/categories/:category"
            render={props =>
              this.state.isAdmin ? (
                <SettingsCat {...props} />
              ) : (
                <AccessError
                  state={"admin"}
                  isLoggedin={this.state.isLoggedin}
                />
              )
            }
          />

          <Route
            path="/sellers/:username"
            render={props =>
              this.state.isSellers ? (
                <SellersDashboard {...props} logout={this.logout} />
              ) : (
                <AccessError
                  state={"sellers"}
                  isLoggedin={this.state.isLoggedin}
                />
              )
            }
          />

          <Route
            path="/checkout"
            render={props => (
              <Checkout
                {...props}
                cartFunction={this.cartFunction}
                cart={this.state.cart}
                isLoggedin={this.state.isLoggedin}
                username={this.state.username}
              />
            )}
          />
          <Route
            path="/dashboard/:username"
            render={props =>
              !this.state.isSellers &&
              !this.state.isAdmin &&
              this.state.isLoggedin ? (
                <UserDashboard
                  {...props}
                  username={this.state.username}
                  logout={this.logout}
                />
              ) : (
                <AccessError
                  state={"user"}
                  isLoggedin={this.state.isLoggedin}
                />
              )
            }
          />
          <Route
            path="/admin/orders"
            render={props =>
              this.state.isAdmin ? (
                <Orders
                  {...props}
                  username={this.state.username}
                  user={"admin"}
                />
              ) : (
                <AccessError
                  state={"admin"}
                  isLoggedin={this.state.isLoggedin}
                />
              )
            }
          />
        </Router>
      </>
    );
  }
}

export default App;
