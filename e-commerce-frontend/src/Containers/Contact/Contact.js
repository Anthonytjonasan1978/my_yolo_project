import React, { useState } from "react";
import axios from "axios";
import { URL } from "../../config";
import Modal from "../../Components/Modal/Modal";
import Loader from "../../Components/Loader/Loader";

const Contact = props => {
  const [values, setValues] = useState({
    fullName: "",
    email: "",
    message: ""
  });
  const [openModal, setOpenModal] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);

  const handleChange = e => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = e => {
    setOpenModal(true)
    e.preventDefault();
    axios
      .post(`${URL}/emails/sendMessage`, {
        fullName: values.fullName,
        email: values.email,
        message: values.message
      })
      .then(res => {
        if (res.data.ok) {
          setIsLoaded(true);
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <div id="contact">
      <h1>
        Any doubts? <br />
        Ask away.
      </h1>
      <form onSubmit={handleSubmit}>
        <h4>Full Name:</h4>
        <input onChange={handleChange} name="fullName"></input>
        <h4>Email:</h4>
        <input onChange={handleChange} name="email"></input>
        <h4>Message:</h4>
        <textarea onChange={handleChange} name="message"></textarea>
        <button className="yellowbutton">SEND</button>
      </form>
      {openModal ? (
        <Modal>
          <div
            style={{
              width: "50vw",
              height: "50vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
              color: "#5c636e"
            }}
          >
            {!isLoaded ? (
              <Loader />
            ) : (
              <>
                <h2>Yay your email is sent! </h2>
                <span role="img" aria-label="party" style={{ fontSize: "2em" }}>
                  🎉
                </span>
                <h4
                  onClick={() => window.location.reload(true)}
                  className="yellowbutton"
                  style={{ marginTop: "2em" }}
                >
                  BACK TO HOME
                </h4>
              </>
            )}
          </div>
        </Modal>
      ) : null}
    </div>
  );
};
export default Contact;
