import React, {useState, useEffect} from "react";
import {NavLink} from 'react-router-dom';
import "./Home.css";
import HowItWorks from "../HowItWorks/HowItWorks";
import Contact from "../Contact/Contact";
import Footer from '../../Components/Footer/Footer';
import Loader from '../../Components/Loader/Loader';
// import Food from '../../Assets/food.jpg';
import Golf from'../../Assets/golf.jpg';
const Home = props => {
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoaded(true);
    }, 500);
    return () => clearTimeout(timer);
  }, []);

  let page;

  if (!isLoaded) {
    page = <Loader />
  } else {
    page = 
    <div id="home" style={{ background: `url(${Golf})`, backgroundSize: 'cover', backgroundPosition: 'center'}}>
    <div className="Header">
      <h1>Keep your eyes on the stars<br/>and your feet on the ground.</h1>
      <NavLink to="/menu" className="yellowbutton" style={{ color: 'black', fontSize: '1.5em'}}>Our Offers <i className="fas fa-arrow-right"></i></NavLink>
    </div>
    <HowItWorks className="Howitworks" />
    <Contact className="Contact" />
    <Footer />
  </div>
  }
  return (
    <>{page}</>
  );
};
export default Home;