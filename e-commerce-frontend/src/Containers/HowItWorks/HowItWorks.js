import React from 'react';
import Card from '../../Assets/card.svg';
import Search from '../../Assets/search.svg';
import Setup from '../../Assets/setup.svg';
import Adventure from '../../Assets/adventure.svg';
import {NavLink} from 'react-router-dom';

const HowItWorks = props => {
    return (
        <div id="how-it-works">
            <h1>HOW IT WORKS.</h1>
            <div className="howitworksContainer">
            <div>
                <img src={Setup} alt="step"/>
                <p>REGISTER OR SIGN IN.</p>
                <NavLink to="/register/user" className="yellowbutton" style={{ color: '#393e46'}}>SIGN IN HERE</NavLink>
            </div>
            <div>
                <img src={Search} alt="step"/>
                <p>CHOOSE YOUR CHALLENGE.</p>
                <NavLink to="/menu" className="yellowbutton" style={{ color: '#393e46'}}>LOOK FOR CHALLENGE</NavLink>
            </div>
            <div>
                <img src={Card} alt="step"/>
                <p>BOOK AND PAY.</p>
            </div>
            <div>
                <img src={Adventure} alt="step"/>
                <p>PACK YOUR BAGS.
                <br/>ENJOY YOUR CHALLENGE.</p>
            </div>
            </div> 
        </div>
    );
}
export default HowItWorks;