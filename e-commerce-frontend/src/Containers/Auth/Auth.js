import React, { useState, useEffect } from "react";
import axios from "axios";
import { URL } from "../../config";

import Owner from "../../Assets/owner.svg";
import Finish from "../../Assets/finish.svg";
import Admin from "../../Assets/admin.svg";
import Loader from "../../Components/Loader/Loader";
import Modal from "../../Components/Modal/Modal";
import "./Auth.css";

const Auth = props => {
  const [isSignup, setIsSignup] = useState(false);
  const [isSellers, setIsSellers] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [authValues, setAuthValues] = useState({
    email: "",
    username: "",
    password: "",
    password2: ""
  });
  const [errorMessage, setErrorMessage] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  // const [openModal, setOpenModal] = useState(false);
  const [sendingMail, setSendingMail] = useState("");

  const submitHandler = e => {
    const { email, username, password, password2 } = authValues;
    console.log(URL)
    e.preventDefault();
    if (isSignup) {
      axios
        .post(`${URL}/users/register`, {
          email,
          username,
          password,
          password2,
          isSellers,
          isAdmin
        })
        .then(res => {
          if (res.data.ok) {
            setAuthValues({
              email: "",
              username: "",
              password: "",
              password2: ""
            });
            setIsSignup(false);
            setErrorMessage("");
          } else {
            res.data.error.errmsg.includes("E11000")
              ? setErrorMessage(
                  "This username is taken. Please choose another one"
                )
              : setErrorMessage(res.data.message);
          }
        })
        .catch(err => console.log(err));
    } else {
      axios
        .post(`${URL}/users/login`, {
          username,
          password,
          isSellers,
          isAdmin
        })
        .then(res => {
          if (res.data.ok) {
            props.login(
              res.data.token,
              res.data.isSellers,
              res.data.isAdmin,
              res.data.username
            );
          } else {
            setErrorMessage(res.data.message);
          }
        })
        .catch();
    }
  };

  const passwordSubmit = e => {
    setSendingMail("sending");
    e.preventDefault();
    axios
      .post(`${URL}/emails/resetPasswordMail`, {
        email: authValues.email,
        newPassword: authValues.password,
        newPassword2: authValues.password2
      })
      .then(res => setSendingMail("Finished"))
      .catch(err => console.log(err));
  };

  const changeHandler = e => {
    setAuthValues({ ...authValues, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    props.match.params.userState === "sellers"
      ? setIsSellers(true)
      : setIsSellers(false);
    props.match.params.userState === "admin"
      ? setIsAdmin(true)
      : setIsAdmin(false);
  }, [props.match.params.userState]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoaded(true);
    }, 500);
    return () => clearTimeout(timer);
  }, []);

  let page;

  if (!isLoaded) {
    page = <Loader />;
  } else {
    if (localStorage.getItem("token") === null) {
      page = (
        <div className="AuthContainer">
          {isSellers ? (
            <img src={Owner} alt="sellers" className="Image" />
          ) : isAdmin ? (
            <img src={Admin} alt="admin" className="Image" />
          ) : (
            <img src={Finish} alt="finish" className="Image" />
          )}
          <form onSubmit={submitHandler} className="AuthForm">
            {isSignup && isSellers ? (
              <h1>
                You Exploit. <br /> Therefore, you are.
              </h1>
            ) : (!isSignup && isSellers) ||
              (!isSignup && !isSellers) ||
              (!isSignup && isAdmin) ? (
              <>
                <h1>
                  Welcome back! <br /> We've missed you.
                </h1>
              </>
            ) : isSignup && isAdmin ? (
              <h1>You control everything.</h1>
            ) : (isSignup && !isSellers) || (isSignup && !isAdmin) ? (
              <h1>
                Still... Nothing beats a <br /> new chalenge.
              </h1>
            ) : null}
            {isSignup ? (
              <div>
                <label>Email:</label>
                <input
                  name="email"
                  value={authValues.email}
                  type="email"
                  onChange={changeHandler}
                ></input>
              </div>
            ) : null}
            <div>
              <label>Username:</label>
              <input
                name="username"
                type="username"
                value={authValues.username}
                onChange={changeHandler}
              ></input>
            </div>
            <div>
              <div className="passwordArea">
                <label>Password:</label>
                {!isSignup ? (
                  <label
                    className="link"
                    onClick={() => setSendingMail("wait")}
                  >
                    Forgot Password?
                  </label>
                ) : null}
              </div>
              <input
                name="password"
                type="password"
                value={authValues.password}
                onChange={changeHandler}
              ></input>
            </div>
            {isSignup ? (
              <div>
                <label>Confirm Password:</label>
                <input
                  name="password2"
                  type="password"
                  value={authValues.password2}
                  onChange={changeHandler}
                ></input>
              </div>
            ) : null}
            {errorMessage ? (
              <span className="errormessage">{errorMessage}</span>
            ) : null}
            <button type="submit" className="AuthButton">
              {isSignup ? `REGISTER` : `SIGN IN`}
            </button>
            {!isAdmin ? (
              <p onClick={() => setIsSignup(prevState => !prevState)}>
                {isSignup
                  ? `Already have an account?`
                  : `Don't have an account?`}
              </p>
            ) : null}
          </form>
          {/* RESET PASSWORD MODAL */}
          {sendingMail === "wait" ? (
            <Modal>
              <form onSubmit={passwordSubmit} className="forgotPasswordForm">
                <h2>RESET YOUR PASSWORD</h2>
                <label>
                  Email: <input onChange={changeHandler} name="email"></input>
                </label>
                <label>
                  Password:{" "}
                  <input onChange={changeHandler} name="password" type="password"></input>
                </label>
                <label>
                  Confirm Password:{" "}
                  <input onChange={changeHandler} name="password2" type="password"></input>
                </label>
                <button className="yellowbutton">RESET PASSWORD</button>
              </form>
            </Modal>
          ) : sendingMail === "sending" ? (
            <Modal>
              <div className="forgotPasswordForm">
                <Loader />
              </div>
            </Modal>
          ) : sendingMail === "finished" ? (
            <Modal>
              <div className="forgotPasswordForm">
                <h1 style={{ textAlign: "center" }}>
                  Yay your password has been resetted{" "}
                  <span
                    role="img"
                    aria-label="party"
                    style={{ fontSize: "2em" }}
                  >
                    🎉
                  </span>
                </h1>
                <h3>A confirmation email has been sent to you.</h3>
                <h3
                  onClick={() => window.location.reload(true)}
                  className="yellowbutton"
                  style={{ fontSize: "1.5em" }}
                >
                  BACK TO SIGN IN
                </h3>
              </div>
            </Modal>
          ) : null}
        </div>
      );
    }
  }

  return <>{page}</>;
};

export default Auth;
