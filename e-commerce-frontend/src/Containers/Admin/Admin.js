import React, { useState, useEffect } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";
import {URL} from '../../config';

import Products from "../../Components/Products/Products";
import ErrorPage from '../../Components/Error/404';
import Loader from '../../Components/Loader/Loader';
import "./Admin.css";

const Admin = props => {
  const [categoriesDB, setCategoriesDB] = useState([]);
  const [category, setCategory] = useState("");
  const [show, setShow] = useState("");
  const [errorPage, setErrorPage] = useState(false);
  const [finishRender, setFinishRender]= useState(false);

  const handleSubmit = e => {
    e.preventDefault();
    let temp = category.trim();
    if (category.length !== 0 && temp.length !== 0) {
      axios
        .post(`${URL}/categories/add`, { category })
        .then(res => {
          let tempArr = [...categoriesDB];
          tempArr.push(res.data);
          setCategoriesDB(tempArr);
          setCategory("");
        })
        .catch(err => console.log(err));
    }
  };

  const handleChange = e => {
    setCategory(e.target.value);
  };

  useEffect(() => {
    axios
      .get(`${URL}/categories/findAll`)
      .then(res => {
        setCategoriesDB(res.data);
        setShow(res.data[0].category);
      })
      .catch(err => console.log(err));
  }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      setFinishRender(true);
    }, 1000);
    return () => clearTimeout(timer);
  }, []);
  
  let page;

  if (errorPage) {
    page = <ErrorPage />
  } else if (!finishRender) {
    page =  <Loader />
  } else {
    page = <div className="AdminContainer">
    <div className="flexWhen700px">
      <h1>Categories</h1>
      <div className="CategoriesBox">
      {categoriesDB.map(el => (
        <div key={el._id} className="CategoriesContainer">
          <span
            className="link"
            onClick={() => setShow(el.category)}
            style={{
              color: show === el.category ? "#393e46" : "black",
              fontWeight: show === el.category ? "bold" : "normal"
            }}
          >
            {el.category}
          </span>
          {show === el.category ? (
            <NavLink to={`/admin/settings/categories/${show}`}>
              <i className="fas fa-cog"></i>
            </NavLink>
          ) : null}
        </div>
      ))}
      </div>
      <form onSubmit={handleSubmit} className="CategoriesForm">
        <input onChange={handleChange} value={category}></input>
        <span className="link">+</span>
      </form>
    </div>
    <Products category={show} isAdmin={props.isAdmin} username={props.username}/>
  </div>
  }

  return <>{page}</>;
};

export default Admin;
