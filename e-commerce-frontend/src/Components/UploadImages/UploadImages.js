import React from "react";
import widgetStyle from "./widgetstyle";
import {URL} from '../../config';


export default class UploadImages extends React.Component {
  uploadWidget = () => {
     window.cloudinary.openUploadWidget(
      {
        cloud_name: "dom04nwee",
        upload_preset: "qqrx2jdd",
        tags: ["user"],
        stylesheet: widgetStyle
      },

      (error, result) => {
        fetch(`${URL}/upload`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            photo_url: result[0].secure_url,
            public_id: result[0].public_id
          })
        })
          .then(response => {
            return response.json();
          })
          .then(responseJson => {
            this.props.url(responseJson.photo_url);
          })
          .catch(e => {});
      }
    );
  };

  render() {
    return (
      <div style={{ border: '1px solid black', padding: '0.5em', width: '13em', marginTop: '1em', textAlign: 'center' }}>
          <h4 className="link" onClick={this.uploadWidget} style={{ margin: '0'}}>
            {" "}
            Upload Image Here <i className="fas fa-upload"></i>
          </h4>
      </div>
    );
  }
}
