import React, { useState, useEffect } from "react";
import axios from "axios";
import Uploader from "../UploadImages/UploadImages";
import {URL} from '../../config';

import "./Settings.css";

const Settings = props => {
  const [product, setProduct] = useState({});
  const [newName, setNewName] = useState("");
  const [newPrice, setNewPrice] = useState(0);
  const [newDescription, setNewDescription] = useState("");
  const [newCategory, setNewCategory] = useState("");
  const [newStock, setNewStock] = useState(0);
  const [newImage, setNewImage] = useState("");
  const [showImage, setShowImage] = useState(false);
  const { name, price, description, category, stock, image } = product;

  useEffect(() => {
    axios
      .get(`${URL}/products/findOneProduct`, {
        params: { _id: props.match.params.id }
      })
      .then(res => setProduct(res.data))
      .catch(err => console.log(err));
  }, [props.match.params.id]);

  const getUrl = url => {
    setNewImage(url);
  };

  useEffect(() => {
    if (newImage) {
      setShowImage(true);
    }
  }, [newImage]);

  const handleSubmit = e => {
    e.preventDefault();
    axios
      .post(`${URL}/products/update`, {
        _id: props.match.params.id,
        updated: {
          name: newName || name,
          price: newPrice || price,
          description: newDescription || description,
          category: newCategory || category,
          stock: newStock || stock,
          image: newImage || image
        }
      })
      .then(res => props.history.goBack())
      .catch(err => console.log(err));
  };

  const removeItem = () => {
    axios
      .post(`${URL}/products/remove`, {
        _id: props.match.params.id
      })
      .then(res => {
        if (res.data.deletedCount === 1) {
          props.history.goBack();
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <>
      <div className="SettingsContainer">
        <div>
          <img src={product.image} alt="product" width="300px" height="300px" />
          {showImage ? (
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-evenly",
                width: "80%",
                paddingTop: '2em'
              }}
            >
              <img src={newImage} alt="product" width="50px" height="50px" />
              <span>
                Image uploaded. <i className="far fa-check-circle"></i>
              </span>
            </div>
          ) : (
            <>
              <h3>Change Image: </h3>
              <Uploader url={getUrl} />
            </>
          )}
        </div>
        <div>
          <h1>Settings</h1>
          <form onSubmit={handleSubmit}>
            <div className="FormColumn">
              <div>
                <h3>
                  Name:{" "}
                  <input
                    placeholder={`${product.name}`}
                    onChange={e => setNewName(e.target.value)}
                  ></input>
                </h3>
                <h3>
                  Price:{" "}
                  <input
                    placeholder={`${product.price}`}
                    onChange={e => setNewPrice(e.target.value)}
                  ></input>
                </h3>
                <h3>
                  Description:{" "}
                  <textarea
                    placeholder={`${product.description}`}
                    onChange={e => setNewDescription(e.target.value)}
                  ></textarea>
                </h3>
              </div>
              <div>
                <h3>
                  Category:{" "}
                  <input
                    placeholder={`${product.category}`}
                    onChange={e => setNewCategory(e.target.value)}
                  ></input>
                </h3>
                <h3>
                  Stock:{" "}
                  <input
                    placeholder={`${product.stock}`}
                    onChange={e => setNewStock(e.target.value)}
                  ></input>
                </h3>
              </div>
            </div>
            <button
              className="yellowbutton"
              style={{ fontSize: "1.2em", float: "right" }}
            >
              SAVE CHANGES
            </button>
          </form>
          <h3
            className="RemoveItem link"
            onClick={() => removeItem(product._id)}
          >
            REMOVE PRODUCT
          </h3>
        </div>
      </div>
    </>
  );
};

export default Settings;
