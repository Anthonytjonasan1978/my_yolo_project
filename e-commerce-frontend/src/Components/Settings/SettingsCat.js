import React, { useState, useEffect } from "react";
import Editable from "../../Assets/editable.svg";
import Modal from "../../Components/Modal/Modal";
import axios from "axios";
import {URL} from '../../config';

const Settings = props => {
  const [newCategory, setNewCategory] = useState("");
  const [warning, setWarning] = useState(false);
  const [productsWarning, setProductsWarning] = useState([]);

  const category = props.match.params.category;

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const res_1 = await axios.post(
        `${URL}/categories/update`,
        {
          category,
          newCategory
        }
      );
      if (res_1.data.nModified === 1) {
        await axios.post(`${URL}/products/updateCategory`, {
          category,
          newCategory
        });
      }
      props.history.push("/admin");
    } catch (err) {
      console.log(err);
    }
  };

  const handleDeleteCat = async () => {
    try {
      const res = await axios.post(`${URL}/categories/remove`, {
        category
      });
      if (res.data.deletedCount === 1) {
        await axios.post(`${URL}/products/removeMany`, {
          category
        });
      }
      props.history.push("/admin");
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    axios
      .get(`${URL}/products/findProductsOfCategory`, {
        params: { category }
      })
      .then(res => {
        setProductsWarning(res.data);
      })
      .catch(err => console.log(err));
  }, [category]);

  return (
    <div className="CategorySettings">
      <img src={Editable} alt="editable" />
      <div>
        <h1>
          SETTINGS <span className="thin">| {category.toUpperCase()}</span>
        </h1>
        <form onSubmit={handleSubmit}>
          <h3>
            Rename Category:{" "}
            <input
              placeholder={category}
              onChange={e => setNewCategory(e.target.value)}
            ></input>
          </h3>
          <button
            className="yellowbutton"
            style={{ fontSize: "1.2em", float: "right" }}
          >
            SAVE
          </button>
        </form>
        <h4
          className="RemoveItem link"
          onClick={
            productsWarning.length !== 0
              ? () => setWarning(true)
              : handleDeleteCat
          }
        >
          REMOVE CATEGORY
        </h4>
        {warning ? (
          <Modal>
            <div>
              <h1>WARNING!</h1>
              <p>
                You have{" "}
                {productsWarning.length === 1
                  ? `this product`
                  : `these products`}{" "}
                in this category.{" "}
              </p>
              <p>
                Once you delete this category,{" "}
                {productsWarning.length === 1
                  ? `this product`
                  : `all of these products`}{" "}
                will be removed.
              </p>
              <div className="productsInCat">
                {productsWarning.map(el => (
                  <div key={el._id}>
                    <p>{el.name}</p>
                  </div>
                ))}
              </div>
              <strong style={{ fontSize: "1.2em" }}>
                ARE YOU SURE YOU WANT TO REMOVE THIS CATEGORY?
              </strong>
              <div className="ActionCall">
                <p onClick={() => setWarning(false)} className="link">
                  I changed my mind
                </p>
                <h5 className="RemoveItem link" onClick={handleDeleteCat}>
                  DELETE '{category}'
                </h5>
              </div>
            </div>
          </Modal>
        ) : null}
      </div>
    </div>
  );
};

export default Settings;
