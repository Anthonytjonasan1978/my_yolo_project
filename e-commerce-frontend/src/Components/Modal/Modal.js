import React from 'react';
import './Modal.css';

const SellersModal = props => {
    return (
        <div className="ModalContainer">
            <div>{props.children}</div>
        </div>
    )
}

export default SellersModal;