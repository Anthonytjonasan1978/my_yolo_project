import React from 'react';
import './Footer.css'

const Footer = props => {
    return (
        <footer className="Footer">
            <h3>Built by Tony Tjon A San</h3>
            <p>@ Barcelona Code School</p>
            <p>&copy; 2019 YOLO Co.</p>
        </footer>
    )
}

export default Footer;