import React from 'react';
import './Loader.css';

const Loader = props => {
    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100vw', height: '100vh'}}>
            <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
        </div>
        
    )
}

export default Loader;