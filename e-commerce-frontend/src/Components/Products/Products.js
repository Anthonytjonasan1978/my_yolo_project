import React, { useState, useEffect } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";
import {URL} from '../../config';

import Empty from "../../Assets/emptycart.svg";
import Modal from "../Modal/Modal";
import ErrorPage from "../Error/404";
import Loader from "../Loader/Loader";
import Uploader from "../UploadImages/UploadImages";
import './Products.css'

const Products = props => {
  const [productsDB, setProductsDB] = useState([]);
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const [stock, setStock] = useState("");
  const [showForm, setShowForm] = useState(false);
  const [showProductInfo, setShowProductInfo] = useState(false);
  const [sellersDescription, setSellersDescription] = useState("");
  const [finishRender, setFinishRender] = useState(false);
  const [oneProduct, setOneProduct] = useState({});
  const [errorPage, setErrorPage] = useState(false);
  const [showImage, setShowImage] = useState(false);

  const { category, isAdmin, username } = props;

  let index = productsDB.findIndex(el => el.category === category);
  const handleSubmit = e => {
    e.preventDefault();
    axios
      .post(`${URL}/products/add`, {
        name,
        price,
        description,
        category,
        image,
        stock,
        sellers: username
      })
      .then(res => {
        let tempArr = [...productsDB];
        tempArr.push(res.data);
        setProductsDB(tempArr);
        setShowForm(false);
        setName("");
        setPrice("");
        setDescription("");
        setStock("");
      })
      .catch(err => console.log(err));
  };

  const getUrl = url => {
    setImage(url);
  };

  useEffect(() => {
    if (image) {
      setShowImage(true);
    }
  }, [image]);

  const getOneProduct = _id => {
    axios
      .get(`${URL}/products/findOneProduct`, { params: { _id } })
      .then(res => {
        setOneProduct(res.data);
      })
      .catch(err => setErrorPage(true));
  };

  const getSellersDescription = sellers => {
    axios
      .get(`${URL}/users/findOneUser`, {
        params: { username: sellers }
      })
      .then(res => setSellersDescription(res.data.userDescription))
      .catch(err => setErrorPage(true));
  };

  useEffect(() => {
    const getData = async () => {
      const res = await axios.get(
        `${URL}/products/findAllProducts`
      );
      await setProductsDB(res.data);
    };
    getData();
  }, [props.username]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setFinishRender(true);
    }, 200);
    return () => clearTimeout(timer);
  }, []);

  let page;

  if (errorPage) {
    page = <ErrorPage />;
  } else {
    page = (
      <div>
        <div className="ProductHeader">
          <h1>{category}</h1>
          <div>
            {isAdmin ? (
              <p
                className="yellowbutton link"
                onClick={() => setShowForm(true)}
                style={{ textAlign: "right" }}
              >
                ADD PRODUCT
              </p>
            ) : null}
            {showForm ? (
              <>
                <Modal style={{ height: "80vh" }}>
                  <span
                    onClick={() => setShowForm(false)}
                    style={{ float: "right" }}
                  >
                    X
                  </span>
                  {showImage ? (
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-evenly",
                        width: "40%"
                      }}
                    >
                      <img
                        src={image}
                        alt="product"
                        width="50px"
                        height="50px"
                      />
                      <span>
                        Image uploaded. <i className="far fa-check-circle"></i>
                      </span>
                    </div>
                  ) : (
                    <Uploader url={getUrl} />
                  )}
                  <form onSubmit={handleSubmit}>
                    <div>
                      Name:{" "}
                      <input
                        value={name}
                        onChange={e => setName(e.target.value)}
                      ></input>
                    </div>
                    <div
                      style={{
                        display: "grid",
                        gridTemplateColumns: "1fr 1fr"
                      }}
                    >
                      <div style={{ width: "80%" }}>
                        Price:{" "}
                        <input
                          value={price}
                          onChange={e => setPrice(e.target.value)}
                        ></input>
                      </div>
                      <div style={{ width: "80%" }}>
                        Stock:{" "}
                        <input
                          value={stock}
                          onChange={e => setStock(e.target.value)}
                        ></input>
                      </div>
                    </div>
                    <div>
                      Description:{" "}
                      <input
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                      ></input>
                    </div>

                    <button>ADD PRODUCT</button>
                  </form>
                </Modal>
              </>
            ) : null}
          </div>
        </div>
        <div className="Product">
          {!finishRender ? (
            <div
              style={{
                width: "70vw",
                height: "70vh",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Loader />
            </div>
          ) : index === -1 ? (
            <div className="EmptyProduct">
              <img src={Empty} alt="empty" />
              <h1>It seems like there's no products in this category</h1>
            </div>
          ) : (
            productsDB.map(el =>
              category === el.category ? (
                <div key={el._id} className="ProductItem">
                  <div className="product-image">
                    <img src={el.image} alt="product"></img>
                  </div>
                  <div className="ProductInfo">
                    <div>
                      <p style={{ fontWeight: "700" }}>{el.name}</p>
                      <p style={{ fontWeight: "500" }}>{el.price} €</p>
                    </div>
                    <h4 style={{ margin: "0", marginBottom: "0.3em" }}>
                      Made by {el.sellers}
                    </h4>

                    <p>
                      {el.description.length <= 30
                        ? el.description
                        : `${el.description.slice(0, 30)} ...`}
                    </p>
                    {isAdmin ? (
                      <>
                        <p>Stock: {el.stock}</p>
                        <NavLink to={`/settings/products/${el._id}`}>
                          <i className="fas fa-cog"></i>
                        </NavLink>
                      </>
                    ) : // eslint-disable-next-line eqeqeq
                    el.stock == 0 ? (
                      <>
                        <h5 style={{ color: "red" }}>SOLD OUT.</h5>
                        <span
                          className="moreInfo"
                          onClick={() => {
                            setShowProductInfo(true);
                            getOneProduct(el._id);
                            getSellersDescription(el.sellers);
                          }}
                        >
                          MORE INFO <i className="fas fa-arrow-right"></i>
                        </span>
                      </>
                    ) : (
                      <>
                        <h5 style={{ color: "green" }}>AVAILABLE.</h5>
                        <div className="product-lastchild"
                        >
                          <span
                            className="moreInfo"
                            onClick={() => {
                              setShowProductInfo(true);
                              getOneProduct(el._id);
                              getSellersDescription(el.sellers);
                            }}
                          >
                            MORE INFO <i className="fas fa-arrow-right"></i>
                          </span>
                          <span
                            className="yellowbutton link"
                            onClick={() =>
                              props.addToCart(el._id, el.name, el.stock)
                            }
                          >
                            ADD TO CART
                          </span>
                        </div>
                      </>
                    )}
                  </div>
                </div>
              ) : null
            )
          )}
        </div>
        {/* Modal for more product info */}
        {showProductInfo ? (
          <Modal>
            <span
              className="link"
              style={{ float: "right" }}
              onClick={() => {
                setShowProductInfo(false);
              }}
            >
              CLOSE
            </span>
            <div
              style={{ display: "flex", clear: "both", alignItems: "center" }}
            >
              <div className="product-image" style={{ paddingRight: "1em" }}>
                <img src={oneProduct.image} alt="product" />
              </div>
              <div>
                <h3 style={{ fontWeight: "700" }}>{oneProduct.name}</h3>
                <p style={{ fontWeight: "500" }}>{oneProduct.price} €</p>
                <em>{oneProduct.description}</em>
              </div>
            </div>
            <div>
              <h3>About the sellers</h3>
              <h4 style={{ marginBottom: "0.3em" }}>
                Sellers {oneProduct.sellers} says:{" "}
              </h4>
              <em>
                "
                {!sellersDescription
                  ? `${oneProduct.sellers} made this with love.`
                  : sellersDescription}
                "
              </em>
            </div>
          </Modal>
        ) : null}
      </div>
    );
  }

  return <>{page}</>;
};

export default Products;
