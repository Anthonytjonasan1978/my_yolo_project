import React, { useState, useEffect } from "react";
import axios from "axios";
import Orders from "../Orders/Orders";
import AccountDetails from "../Account/AccountDetails";
import AccountSettings from "../Account/AccountSettings";
import { URL } from "../../config";

const UserDashboard = props => {
  const [showInfo, setShowInfo] = useState("orders");
  const [response, setResponse] = useState({ address: "" });

  useEffect(() => {
    axios
      .get(`${URL}/users/findOneUser`, {
        params: { username: props.username }
      })
      .then(res => setResponse(res.data))
      .catch(err => console.log(err));
  }, [props]);

  return (
    <div className="userDashboard">
      <div className="userSidebar">
        <h3 className="link" onClick={() => setShowInfo("orders")} style={{ color: showInfo === 'orders' ? '#f8b500' : '#393e46'}}>My Orders</h3>
        <h3 className="link" onClick={() => setShowInfo("accountDetails")} style={{ color: showInfo === 'accountDetails' ? '#f8b500' : '#393e46'}}>Account Details</h3>
        <h3 className="link" onClick={() => setShowInfo("settings")} style={{ color: showInfo === 'settings' ? '#f8b500' : '#393e46'}}>Settings</h3>
      </div>
      <div className="userMain">
        {showInfo === "settings" ? (
          <AccountSettings {...props} response={response} />
        ) : showInfo === "accountDetails" ? (
          <AccountDetails {...props} response={response} />
        ) : (
          <Orders {...props} user={'user'} username={props.username}/>
        )}
      </div>
    </div>
  );
};

export default UserDashboard;
