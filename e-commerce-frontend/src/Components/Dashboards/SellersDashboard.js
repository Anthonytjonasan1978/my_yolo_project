import React, { useState, useEffect } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";

import Modal from "../Modal/Modal";
import DefaultUser from "../../Assets/defaultUser.svg";
import Empty from "../../Assets/emptycart.svg";
import ErrorPage from "../Error/404";
import Loader from "../Loader/Loader";
import Uploader from "../UploadImages/UploadImages";
import AccountSettings from '../Account/AccountSettings';
import Orders from '../Orders/Orders';
import {URL} from '../../config';
import "./SellersDashboard.css";

const SellersDashboard = props => {
  const username = props.match.params.username;
  const [showSection, setShowSection] = useState('products');
  const [categories, setCategories] = useState([]);
  const [userProducts, setUserProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [showDescription, setShowDescription] = useState(false);
  const [image, setImage] = useState("");
  const [showImage, setShowImage] = useState(false);
  const [userDescription, setUserDescription] = useState("");
  const [product, setProduct] = useState({
    name: "",
    price: "",
    description: "",
    category: "",
    stock: ""
  });
  const [isLoaded, setIsLoaded] = useState(false);
  const [challengeIds, setChallengeIds] = useState([]);
  const [sellersOrders, setSellersOrders] = useState([]);

  const handleChange = e => {
    setProduct({ ...product, [e.target.name]: e.target.value });
  };

  const handleProductSubmit = e => {
    const { name, price, description, category, stock } = product;
    e.preventDefault();
    axios
      .post(`${URL}/products/add`, {
        name,
        price,
        description,
        category,
        image,
        stock,
        sellers: username
      })
      .then(res => {
        setShowModal(false);
        window.location.reload(true);
      })
      .catch(err => console.log(err));
  };

  const submitDescription = e => {
    axios
      .post(`${URL}/users/updateSellersDescription`, {
        username,
        userDescription: e.target.firstElementChild.value
      })
      .then()
      .catch(err => console.log(err));
  };

  const getUrl = url => {
    setImage(url);
  };

  useEffect(() => {
    if (image) {
      setShowImage(true);
    }
  }, [image]);

  useEffect(() => {
    axios
      .get(`${URL}/categories/findAll`)
      .then(res => setCategories(res.data))
      .catch(err => console.log(err));
  }, []);

  useEffect(() => {
    let temp = [...challengeIds];
    userProducts.forEach(el => temp.push(el._id))
    setChallengeIds(temp)
  },[userProducts])

  useEffect(() => {
    axios.post(`${URL}/sellersSales/findOrdersOfSellers`, {challengeIds})
    .then(res => {
      setSellersOrders(res.data)
    })
    .catch(err => console.log(err))
  }, [challengeIds])

  useEffect(() => {
    axios
      .get(`${URL}/products/findProductsOfSellers`, {
        params: { sellers: username }
      })
      .then(res => {
        setUserProducts(res.data);
      })
      .catch(err => console.log(err));
  }, [username]);

  useEffect(() => {
    axios
      .get(`${URL}/users/findOneUser`, { params: { username } })
      .then(res => setUserDescription(res.data.userDescription))
      .catch(err => console.log(err));
  }, [username]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoaded(true);
    }, 500);
    return () => clearTimeout(timer);
  }, []);

  let page;
  let prodSection = 
  <>
  <strong style={{ fontSize: "1.5em" }}>Description </strong>
  {!userDescription ? (
    <>
      <p>
        You should add a brief description of your challenges that you have to offer and a short
        story so buyers will have a good idea what they order!
      </p>
      <form onSubmit={submitDescription} style={{ display: 'flex', alignItems: 'flex-end'}}>
        <textarea style={{ width: "70%", fontSize: "1em", marginRight: '2em' }}></textarea>
        <button className="yellowbutton">SUBMIT</button>
      </form>
    </>
  ) : (
    <>
      <span className="link" onClick={() => setShowDescription(true)}>
        | Change Description
      </span>
      <div>
        {!showDescription ? (
          <p>{userDescription}</p>
        ) : (
          <form onSubmit={submitDescription}>
            <textarea
              style={{
                width: "95%",
                fontSize: "1em",
                margin: "1em 0"
              }}
              placeholder={userDescription}
            ></textarea>
            <div style={{ textAlign: "right", marginRight: '1em' }}>
              <span
                onClick={() => setShowDescription(false)}
                className="link"
                style={{
                  opacity: "0.7",
                  paddingRight: "0.5em",
                  fontSize: "0.8em"
                }}
              >
                CANCEL
              </span>
              <button className="yellowbutton">UPDATE</button>
            </div>
          </form>
        )}
      </div>
    </>
  )}
  <div className="SellersProductContainer">
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
      }}
    >
      <h3>Your Products: </h3>
      <p className="yellowbutton" onClick={() => setShowModal(true)}>
        ADD PRODUCT
      </p>
    </div>
    {showModal ? (
      <Modal>
        <div>
          <span style={{ fontSize: "1.5em", fontWeight: "bold" }}>
            You're almost there!
          </span>
          <span
            style={{ float: "right" }}
            onClick={() => setShowModal(false)}
            className="link"
          >
            X
          </span>
        </div>
        {showImage ? (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-evenly",
              width: "80%",
              paddingTop: "2em"
            }}
          >
            <img src={image} alt="product" width="50px" height="50px" />
            <span>
              Image uploaded. <i className="far fa-check-circle"></i>
            </span>
          </div>
        ) : (
          <Uploader url={getUrl} />
        )}
        <form onSubmit={handleProductSubmit} className="addProductForm">
          <div>
            <h4>
              Category:{" "}
              <select name="category" onChange={handleChange}>
                <option>CHOOSE...</option>
                {categories.map(el => (
                  <option key={el._id}>{el.category}</option>
                ))}
              </select>
            </h4>
          </div>
          <div>
            <h4>Name:</h4>
            <input name="name" onChange={handleChange}></input>
          </div>
          <div>
            <h4>Price:</h4>
            <input name="price" onChange={handleChange}></input>
          </div>
          <div>
            <h4>Description: </h4>
            <input name="description" onChange={handleChange}></input>
          </div>
          <div>
            <h4>Stock:</h4>
            <input name="stock" onChange={handleChange}></input>
          </div>
          <button className="yellowbutton">ADD PRODUCT</button>
        </form>
      </Modal>
    ) : null}
    <div className="UserProducts">
      {userProducts.length === 0 ? (
        <div>
          <img src={Empty} alt="empty" width="100%"/>
          <h2>You haven't added any products.</h2>
        </div>
      ) : (
        userProducts.map(el => (
          <div key={el._id} className="ProductItem">
            <div className="product-image">
              <img src={el.image} alt="product"></img>
            </div>
            <div className="ProductInfo">
              <div>
                <p style={{ fontWeight: "700" }}>{el.name}</p>
                <p style={{ fontWeight: "500" }}>{el.price} €</p>
              </div>
              <h4 style={{ margin: "0", marginBottom: "0.3em" }}>
                Made by {el.sellers}
              </h4>

              <p>
                {el.description.length <= 30
                  ? el.description
                  : `${el.description.slice(0, 30)} ...`}
              </p>

              <p>Stock: {el.stock}</p>
              <NavLink to={`/settings/products/${el._id}`}>
                <i className="fas fa-cog"></i>
              </NavLink>
            </div>
          </div>
        ))
      )}
    </div>
  </div>
  </>



  if (!isLoaded) {
    page = <Loader />;
  } else {
    page = (
      <div className="SellersDashboard">
        <div className="SellersSidebar">

          <img src={DefaultUser} alt="profile"/>

          <h3 onClick={() => setShowSection('products')} className="link" style={{ color: showSection === 'products' ? '#f8b500' : '#393e46'}}>My Products</h3>
          <h3 onClick={() => setShowSection('settings')} className="link" style={{ color: showSection === 'settings' ? '#f8b500' : '#393e46'}}>Account Settings</h3>
          <h3 onClick={() => setShowSection('salesHistory')} className="link" style={{ color: showSection === 'salesHistory' ? '#f8b500' : '#393e46'}}>Sales History</h3>
        </div>
        <div className="SellersMain">
          <h1>Welcome {username}</h1>
          {showSection === 'settings' 
            ? <AccountSettings username={username} sellers={'sellers'} {...props}/>
            : showSection === 'salesHistory' ? <Orders user={'sellers'} sellersOrders={sellersOrders} challengeIds={challengeIds}/> 
            : prodSection}
        </div>
      </div>)
  }

  return <>{page}</>;
};

export default SellersDashboard;
