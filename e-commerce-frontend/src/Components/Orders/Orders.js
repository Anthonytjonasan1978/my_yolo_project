import React, { useState, useEffect } from "react";
import axios from "axios";
import { URL } from "../../config";
import "./Orders.css";

const User = props => {
  const [orders, setOrders] = useState([]);
  const [sort, setSort] = useState("oldest to most recent");
  const [showDropdown, setShowDropdown] = useState(false);
  const [sales, setSales] = useState(0);
  const [sellersOrders, setSellersOrders] = useState([]);

  useEffect(() => {
    axios
      .get(`${URL}/orders/getOrdersByUsername/${props.username}`)
      .then(res => {
        setOrders(res.data);
      })
      .catch(err => console.log(err));
  }, [props.username]);

  useEffect(() => {
    axios
      .get(`${URL}/orders/getAllOrders`)
      .then(res => {
        setOrders(res.data);
      })
      .catch(err => console.log(err));
  }, []);

  useEffect(() => {
    setSellersOrders(props.sellersOrders)
  }, [props.sellersOrders])

  useEffect(() => {
    if (props.user === 'sellers') {
      let total = 0;
      props.sellersOrders.forEach(el => total += el.price * el.quantity);
      setSales(total);
    }
  }, [props])

  const handleSort = e => {
    if (e.target.getAttribute("name") === "date-desc") {
      setSort("most recent to oldest");
      axios
        .get(`${URL}/orders/sortOrdersByDate`)
        .then(res => {
          setOrders(res.data);
          setShowDropdown(false);
        })
        .catch(err => console.log(err));
      let arr = sellersOrders.reverse();
      setSellersOrders(arr);
    } else {
      setSort("oldest to most recent");
      axios
        .get(`${URL}/orders/getAllOrders`)
        .then(res => {
          setOrders(res.data);
          setShowDropdown(false);
        })
        .catch(err => console.log(err));
        setSellersOrders(props.sellersSales)
    }
  };

  return (
    <div className="Orders">
      {props.user === "sellers" ? <div>
        <h2>Sales made: <span style={{fontSize: '0.9em'}}>{sales} € </span></h2>
      </div> : <h1>Welcome {props.username}!</h1>}

      <div className="orderHeader">
        <h3>Order History</h3>
        <h3
          className="link"
          onClick={() => setShowDropdown(prevState => !prevState)}
        >
          Sort by <i className="fas fa-chevron-down"></i>
        </h3>
      </div>
      {showDropdown ? (
        <div className="sortDropdown">
          <p
            name="date-asc"
            style={{
              color: sort === "oldest to most recent" ? "#f8b500" : "#f7f7f7"
            }}
            onClick={handleSort}
            className="link"
          >{`Date (Oldest - Most Recent)`}</p>
          <p
            name="date-desc"
            style={{
              color: sort === "most recent to oldest" ? "#f8b500" : "#f7f7f7"
            }}
            onClick={handleSort}
            className="link"
          >{`Date (Most Recent - Oldest)`}</p>
        </div>
      ) : null}

      <div className="OrderContainer">
        <h4>Sorted by {sort}</h4>
        {props.user === "sellers"
          ? sellersOrders.map(el => (
              <div key={el._id}>
                <h3>
                  Ordered by{" "}
                  <span style={{ color: "#f8b500", fontSize: "1.1em" }}>
                    {el.username}
                  </span>{" "}
                  on: {el.date} at {el.time}
                </h3>
                <div key={el.foodId}>
                  <p>{el.name}</p>
                </div>
              </div>
            ))
          : 
        orders.map(el => (
          <div key={el._id}>
            {props.user === "admin" ? (
              <h3>
                Ordered by{" "}
                <span style={{ color: "#f8b500", fontSize: "1.1em" }}>
                  {el.username}
                </span>{" "}
                on: {el.date} at {el.time}
              </h3>
            ) : (
              <h4>
                Ordered on: {el.date} at {el.time}
              </h4>
            )}
            {el.cart.map(item => (
              <div key={item.id}>
                <p>{item.name}</p>
              </div>
            ))}
            <h4>{el.total} €</h4>
            <hr />
          </div>
        ))}
      </div>
    </div>
  );
};

export default User;
