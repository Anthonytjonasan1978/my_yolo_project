import React, { useState } from "react";
import {
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  injectStripe
} from "react-stripe-elements";
import axios from "axios";

import {URL} from '../../config';
import Loader from "../Loader/Loader";
import Modal from "../Modal/Modal";

const Payment = props => {
  const [errorMessage, setErrorMessage] = useState("");
  const [paymentInfo, setPaymentInfo] = useState({
    cardNumber: false,
    cardExpiry: false,
    cardCvc: false
  });
  const [userDetails, setUserDetails] = useState({
    name: "",
    lastname: "",
    email: "",
    phone: "",
    postalCode: ""
  });
  const [showModal, setShowModal] = useState(false);
  const [success, setSuccess] = useState(false);

  const handleChange = ({ elementType, complete, error }) => {
    if (error) {
      setErrorMessage(error.code);
    }
    setPaymentInfo({ ...paymentInfo, [elementType]: complete });
  };

  const handleInputChange = e =>
    setUserDetails({ ...userDetails, [e.target.name]: e.target.value });

  const handleSubmit = async e => {
    e.preventDefault();
    setShowModal(true);
    const { cardNumber, cardCvc, cardExpiry } = paymentInfo;
    if (!cardNumber || !cardCvc || !cardExpiry)
      return alert("Please fill all the fields");
    const fullname = userDetails.name + userDetails.lastname;
    const { name, lastname, email, phone, postalCode } = userDetails;
    if (props.stripe) {
      const { token } = await props.stripe.createToken({
        name: fullname,
        email: userDetails.email
      });
      const response = await axios.post(
        `${URL}/payment/charge`,
        {
          token_id: token.id,
          amount: props.total,
          name,
          lastname,
          email,
          phone,
          postalCode
        }
      );
      response.data.status === "succeeded" ? setSuccess(true) : alert("error");

      if (response.data.status === "succeeded") {
        //create orders
        let total = JSON.parse(localStorage.getItem("total"));
        axios
          .post("http://localhost:4000/orders/addOrders", {
            cart: props.cart,
            total,
            username: props.username
          })
          .then(res => {
            //send emails
            axios
              .post("http://localhost:4000/emails/sendEmail", {
                name: props.username,
                email: userDetails.email,
                subject: "Your order confirmation",
                order: res.data.order
              })
              .then(
                props.cart.forEach(el => {
                  axios.post(`${URL}/chefSales/createChefOrders`, {
                    foodId: el.id,
                    name: el.name,
                    price: el.price,
                    quantity: el.quantity,
                    username: props.username
                  })
                  .then()
                  .catch(err => console.log(err))
                })
              )
              .catch();

            //empty cart and total in localStorage
            localStorage.setItem("cart", JSON.stringify([]));
            localStorage.setItem("total", 0);

            setTimeout(() => props.history.push("/"), 3000);
          })
          .catch(err => console.log(err));

          
      } else {
        alert("Stripe.js hasn't loaded yet.");
      }
    }
  };


  return (
    <div>
      <form className="checkout" onSubmit={handleSubmit}>
        {/*************************** FIRST ROW ****************************/}
        <div className="split-form">
          <h3>
            Name:
            <input
              required
              name="name"
              type="text"
              placeholder="Jane"
              onChange={handleInputChange}
            />
          </h3>
          <h3>
            Last Name:
            <input
              required
              name="lastname"
              type="text"
              placeholder="Doe"
              onChange={handleInputChange}
            />
          </h3>
        </div>
        {/*************************** SECOND ROW ****************************/}
        <div className="split-form">
          <h3>
            Email:
            <input
              required
              name="email"
              type="email"
              placeholder="jane.doe@example.com"
              onChange={handleInputChange}
            />
          </h3>
          <h3>
            Phone number:
            <input
              required
              name="phone"
              type="number"
              placeholder="+34 816463723"
              onChange={handleInputChange}
            />
          </h3>
        </div>
        {/***************************** THIRD ROW *****************************/}
        <div className="split-form">
          <h3>
            Card number:
            <CardNumberElement onChange={handleChange} />
          </h3>
          <h3>
            Expiration date:
            <CardExpiryElement onChange={handleChange} />
          </h3>
        </div>
        {/*************************** FOURTH ROW ****************************/}
        <div className="split-form">
          <h3>
            CVC:
            <CardCVCElement onChange={handleChange} />
          </h3>
          <h3>
            Postal code:
            <input
              name="postalCode"
              type="text"
              placeholder="94115"
              className="StripeElement"
              onChange={handleInputChange}
            />
          </h3>
        </div>
        <div className="error" role="alert">
          {errorMessage}
        </div>
        <button
          className="yellowbutton"
          style={{ width: "90%", fontSize: "1em", marginBottom: '2em' }}
        >
          Pay {props.total} €
        </button>
      </form>
      {showModal ? (
        <Modal>
          <div
            style={{
              width: "50vw",
              height: "50vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            {!success ? (
              <Loader />
            ) : (
              <>
                <p style={{ fontSize: '2em' }}>
                  Yay you did it!{" "}
                  <span role="img" aria-label="party">
                    🎉
                  </span>
                </p>
              </>
            )}
          </div>
        </Modal>
      ) : null}
    </div>
  );
};

export default injectStripe(Payment);
