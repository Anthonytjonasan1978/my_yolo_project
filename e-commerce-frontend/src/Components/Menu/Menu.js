import React, { useState, useEffect } from "react";
import axios from "axios";
import "./Menu.css";
import { NavLink } from "react-router-dom";
import ErrorPage from '../Error/404';
import Loader from '../Loader/Loader';
import {URL} from '../../config';

import Products from "../Products/Products";

const Menu = props => {
  const [categoriesDB, setCategoriesDB] = useState([]);
  const [show, setShow] = useState("");
  const [cart, setCart] = useState([]);
  const [total, setTotal] = useState(0);
  const [quantityLimit, setQuantityLimit] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  // const [errorPage, setErrorPage] = useState(false);

  const addToCart = (id, name, stock) => {
    let temp = JSON.parse(localStorage.getItem('cart'))
    let index = temp.findIndex(el => el.name === name);
    if (index === -1) {
      temp.push({ id, name, quantity: 1 });
      localStorage.setItem("cart", JSON.stringify(temp));
    } else {
      if (temp[index].quantity < stock) {
        temp[index].quantity += 1;
        localStorage.setItem("cart", JSON.stringify(temp));
      } else {
        setQuantityLimit(true);
      }
    }
    setCart(temp);
  };

  const removeItem = name => {
    let temp = [...cart];
    let index = temp.findIndex(el => el.name === name);
    if (index !== -1 && temp[index].quantity === 1) {
      temp.splice(index, 1);
      localStorage.setItem("cart", JSON.stringify(temp));
    } else if (index !== -1 && temp[index].quantity !== 1) {
      temp[index].quantity -= 1;
      localStorage.setItem("cart", JSON.stringify(temp));
      setQuantityLimit(false);
    }
    setCart(temp);
  };

  useEffect(() => {
    axios
      .get(`${URL}/categories/findAll`)
      .then(res => {
        console.log(res,'================')
        setCategoriesDB(res.data);
        setShow(res.data[0].category);
      })
      .catch(err => console.log(err));
  }, []);

  useEffect(() => {
    props.cartFunction();
  });

  useEffect(() => {
    let cart2 = JSON.parse(localStorage.getItem("cart"));
    if (cart2 === null) {
      localStorage.setItem("cart", JSON.stringify([]))
    }
    setCart(cart2);
  }, []);

  useEffect(() => {
    const calculateTotal = () => {
      let tempTotal = 0;
      props.cart.forEach(el => (tempTotal += el.price * el.quantity));
      setTotal(tempTotal);
      localStorage.setItem("total", total);

    };
    calculateTotal();
  }, [props.cart, total]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoaded(true);
    }, 500);
    return () => clearTimeout(timer);
  }, []);

  let page;
  
  if (!isLoaded) {
    page = <Loader />
  } else {
    page = <div className="MenuContainer">
    <div>
      <h1>Categories</h1>
      {categoriesDB.map(el => (
        <div key={el._id} className="CategoriesContainer">
          <span
            className="link"
            onClick={() => setShow(el.category)}
            style={{
              color: show === el.category ? "#f8b500" : "black",
              fontWeight: show === el.category ? "bold" : "normal"
            }}
          >
            {el.category}
          </span>
        </div>
      ))}
    </div>
    <div className="MenuProducts">
      <Products
        category={show}
        addToCart={addToCart}
        quantityLimit={quantityLimit}
      />
    </div>
    <div className="Cart">
      <h2>
        My Cart{" "}
        {props.cart.length !== 0 ? (
          <span role="img" aria-label="face">
            😋
          </span>
        ) : (
          <span role="img" aria-label="face">
            🥺
          </span>
        )}
      </h2>
      {props.cart.length !== 0 ? (
        <>
          {props.cart.map(el => (
            <div className="CartItem" key={el.id}>
              <img src={el.image} alt="product" width="60px" height="60px" />
              <div style={{ display: "flex", fontWeight: 'bold' }}>
                <p style={{ paddingRight: "0.3em" }}>{el.quantity} </p>
                <p>x</p>
                <p style={{ paddingLeft: "0.5em", width: '5em' }}>
                  {el.name}
                </p>
              </div>
              <p style={{ marginLeft: '1em'}}>{el.price}€</p>
              <i
                className="fas fa-minus-circle"
                onClick={() => removeItem(el.name)}
              ></i>
            </div>
          ))}
          {quantityLimit ? <p>NO MORE STOCK AVAILABLE</p> : null}
          <div style={{ textAlign: 'right'}}>
          <h4 style={{ fontSize: '1.5em'}}>Total: {total} €</h4>
          <NavLink to="/checkout" className="yellowbutton">
            Checkout <i className="fas fa-arrow-right"></i>
          </NavLink>
          </div>

        </>
      ) : (
        <div>
          <p>You have nothing in the cart so far.</p>
          <p>Time to change that!</p>
        </div>
      )}
    </div>
  </div>
  }

  return (
    <>{page}</>
  );
};

export default Menu;
