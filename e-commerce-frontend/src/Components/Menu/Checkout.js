import React, { useState, useEffect } from "react";
import Payment from "./Payment";
import axios from 'axios';
import { Elements, StripeProvider } from "react-stripe-elements";
import {pk_test} from "../../config";
import {URL} from '../../config';
import AccountDetails from '../Account/AccountDetails';
import { NavLink } from "react-router-dom";
import Login from '../../Assets/login.svg';
import Loader from '../Loader/Loader';

const Checkout = props => {
  const [total, setTotal] = useState(0);
  const [openPaymentForm, setOpenPaymentForm] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [response, setResponse] = useState(null)

  
  useEffect(() => {
    axios
      .get(`${URL}/users/findOneUser`, {
        params: { username: props.username }
      })
      .then(res => {
        if (res.data.hasOwnProperty("address")) {
          setOpenPaymentForm(true);
          setResponse(res.data)
        }
      })
      .catch(err => console.log(err));
  }, [props.username]);

  useEffect(() => {
    props.cartFunction();
    setTotal(JSON.parse(localStorage.getItem("total")));
  }, [props]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoaded(true);
    }, 500);
    return () => clearTimeout(timer);
  }, []);

  let page;

  if (!isLoaded) {
    page = <Loader />
  } else {
    page = <div className="CheckoutContainer">
    <div>
      <h1>Your Cart</h1>
      {props.cart.map(el => (
        <div key={el.id} className="CartItem checkout">
          <img src={el.image} alt="product" width="60px" height="60px" />
          <div style={{ display: "flex" }}>
            <p>{el.quantity} x</p>
            <p style={{ paddingLeft: "0.5em", width: "80%" }}>{el.name}</p>
          </div>
          <p style={{ textAlign: "right" }}>{el.price} €</p>
        </div>
      ))}
      <h2 style={{ textAlign: "right" }}>Total: {total} €</h2>
    </div>
    {props.isLoggedin ? (
      <div>
        <div style={{ display: 'flex', alignItems: 'flex-end'}}>
        <h1 style={{ marginBottom: '0', paddingRight: '0.5em'}}>CHECKOUT</h1>
        <span>| You're almost there!</span>
        </div>
        <AccountDetails username={props.username} response={response}/>
        {!openPaymentForm ? (
          <>
          
            <div className="PaymentContainer"><h3 style={{ fontSize: "1.6em" }}>Payment</h3></div>
          </>
        ) : (
          <>
            <div className="PaymentContainer">
              <h3 style={{ fontSize: "1.6em", margin: '0.5em 0' }}>Payment</h3>
              <StripeProvider apiKey={pk_test}>
                <div className="PaymentForm">
                  <Elements>
                    <Payment
                      total={total}
                      cart={props.cart}
                      username={props.username}
                      {...props}
                    />
                  </Elements>
                </div>
              </StripeProvider>
            </div>
          </>
        )}
      </div>
    ) : (
      <div style={{ textAlign: 'center'}}>
        <h1>You would need to log in first!</h1>
        <img src={Login} alt="login" style={{ width: '60%'}}/>
        <div style={{ paddingTop: '2em'}}>
        <NavLink to="/register/user" className="yellowbutton">LOGIN HERE</NavLink>
        </div>
      </div>
    )}
  </div>
  }

  return (
    <>{page}</>
  );
};

export default Checkout;
