import React, { useState, useEffect } from "react";
import axios from "axios";
import {URL} from '../../config'

const AccountDetails = props => {
  const [hasAddress, setHasAddress] = useState(false);
  const [changeAddress, setChangeAddress] = useState(false);
  const [street, setStreet] = useState("");
  const [city, setCity] = useState("");
  const [zipCode, setZipCode] = useState("");
  const [address, setAddress] = useState({
    street: "",
    city: "",
    zipCode: ""
  });

  useEffect(() => {
    if (props.response !== null) {
    if (props.response.hasOwnProperty("address")) {
      const { street, city, zipCode } = props.response.address;
      setHasAddress(true);
      setAddress({
        street,
        city,
        zipCode
      });
    }
  }}, [props.response]);

  const handleSubmit = e => {
    e.preventDefault();
    axios
      .post(`${URL}/users/updateAddress`, {
        username: props.username,
        address: {
          street,
          city,
          zipCode
        }
      })
      .then(res => window.location.reload(true))
      .catch(err => console.log(err));
  };

  return (
    <div>
      <h1>Your Address</h1>
      <div>
        {hasAddress && !changeAddress ? (
          <div>
            <h3>
              Street:{" "}
              <span style={{ fontWeight: "400" }}>{address.street}</span>
            </h3>
            <h3>
              City: <span style={{ fontWeight: "400" }}>{address.city}</span>
            </h3>
            <h3>
              ZIP Code:{" "}
              <span style={{ fontWeight: "400" }}>{address.zipCode}</span>
            </h3>
            <p
              className="yellowbutton"
              style={{ width: "30%", textAlign: 'center' }}
              onClick={() => {
                setChangeAddress(true);
              }}
            >
              CHANGE ADDRESS
            </p>
          </div>
        ) : (
          <div>
            <h3 style={{ fontSize: "1.6em" }}>Enter your details</h3>
            <form onSubmit={handleSubmit} className="AddressForm">
              <h4>
                Street Address:{" "}
                <input
                  value={street}
                  onChange={e => setStreet(e.target.value)}
                ></input>
              </h4>
              <h4>
                City:{" "}
                <input
                  value={city}
                  onChange={e => setCity(e.target.value)}
                ></input>
              </h4>
              <h4>
                ZIP Code:{" "}
                <input
                  value={zipCode}
                  onChange={e => setZipCode(e.target.value)}
                ></input>
              </h4>
              <button
                className="yellowbutton"
                style={{
                  float: "right",
                  fontSize: "1em",
                  marginBottom: "1.5em"
                }}
              >
                SET ADDRESS
              </button>
            </form>
          </div>
        )}
      </div>
    </div>
  );
};

export default AccountDetails;
