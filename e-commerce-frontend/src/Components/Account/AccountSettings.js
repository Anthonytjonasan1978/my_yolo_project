import React, { useState, useEffect } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";
import Modal from "../Modal/Modal";
import {URL} from '../../config';
import './Settings.css';

const AccountSettings = props => {
  const [email, setEmail] = useState("");
  const [passwords, setPasswords] = useState({
      oldPassword: '',
      newPassword: '',
      newPassword2: ''
  })
  const [requestLogout, setRequestLogout] = useState(false);
  const [newEmail, setNewEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleEmail = e => {
    e.preventDefault();
    axios
      .post(`${URL}/users/updateEmail`, {
        username: props.username,
        newEmail
      })
      .then(res => setRequestLogout(true))
      .catch(err => console.log(err));
  };

  const handlePassword = e => {
      setPasswords({...passwords, [e.target.name]: e.target.value})
  }

  const handlePwSubmit = e => {
      e.preventDefault();
      axios.post(`${URL}/users/changePassword`, {
          username: props.username, 
          oldPassword: passwords.oldPassword,
          newPassword: passwords.newPassword,
          newPassword2: passwords.newPassword2
      })
      .then(res => {
          if (res.data.ok) {
            setRequestLogout(true)
          } else {
              setErrorMessage(res.data.message)
          }
      })
      .catch(err => console.log(err))
  }

  useEffect(() => {
    if (props.response) {
      setEmail(props.response.email);
    }
  }, [props.response]);

  return (
    <div>
      <form onSubmit={handleEmail}>
        <h1>CHANGE EMAIL</h1>
        <input
          placeholder={email}
          style={{ fontSize: '1.1em', marginRight: '1em', padding: '0.5em'}}
          onChange={e => setNewEmail(e.target.value)}
        ></input>
        <button className="yellowbutton">CHANGE</button>
      </form>
      <form onSubmit={handlePwSubmit}  className="settingsForm">
        <h1>CHANGE PASSWORD</h1>
        <div><h3>OLD PASSWORD</h3><input onChange={handlePassword}type="password"  name="oldPassword"></input></div>
        <div><h3>NEW PASSWORD</h3><input onChange={handlePassword}type="password"  name="newPassword"></input></div>
        <div><h3>CONFIRM NEW PASSWORD</h3><input onChange={handlePassword}type="password"  name="newPassword2"></input></div>
        <button className="yellowbutton">CHANGE</button>
        <p style={{ color: 'red'}}>{errorMessage}</p>
      </form>
      {requestLogout ? (
        <Modal>
          <div
            style={{
              height: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column"
            }}
          >
            <h1>
              <i
                className="far fa-check-circle"
                style={{ fontSize: "2em", color: "#01d28e" }}
              ></i>
            </h1>
            <h1 style={{ textAlign: 'center'}}>Change is successful.</h1>
            <h3 style={{ textAlign: "center" }}>
              For the change to take effect, please logout and login again.
            </h3>
            <NavLink
              className="yellowbutton"
              onClick={() => props.logout()}
              to={`/register/${props.sellers === 'sellers' ? 'sellers' : 'user'}`}
            >
              LOGOUT
            </NavLink>
          </div>
        </Modal>
      ) : null}
    </div>
  );
};

export default AccountSettings;
