import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { HashLink as Link } from "react-router-hash-link";
import "./Navbar.css";
import Cart from "../../Assets/shopping-cart.png";
import User from "../../Assets/profile.png";
import Logo from "../../Assets/logo2.jpeg";

const Navbar = props => {
  const [cartDropdown, setCartDropdown] = useState(false);
  const [userDropdown, setUserDropdown] = useState(false);
  const [showAccounts, setShowAccounts] = useState(false);

  const total = JSON.parse(localStorage.getItem("total"));

  return (
    <div className="Navbar">
      <Link smooth to="/#home">
        <div className="Logo">
          <img src={Logo} alt="logo" />
        </div>
      </Link>
      {!props.isLoggedin ? (
        <div className="NavItemsCenter">
          <Link smooth to="/#how-it-works" className="navItemCenterGone">
            HOW IT WORKS
          </Link>
          <Link smooth to="/#contact" className="navItemCenterGone">
            CONTACT
          </Link>
          <NavLink to="/menu">CHALLENGES</NavLink>
        </div>
      ) : props.isLoggedin && props.isSellers ? (
        <NavLink to={`/sellers/${props.username}`}>DASHBOARD</NavLink>
      ) : props.isLoggedin && !props.isSellers && !props.isAdmin ? (
        <NavLink to="/menu">CHALLENGES</NavLink>
      ) : (
        <>
        <NavLink to="/admin">ADMIN SETTINGS</NavLink>
        <NavLink to="/admin/orders">ORDERS</NavLink>
        </>
      )}
      <div className="NavItemsEnd">
        {!props.isAdmin && !props.isSellers ? (
          <span
            className="link"
            onClick={() => {
              setCartDropdown(prevState => !prevState);
              setUserDropdown(false);
              setShowAccounts(false);
            }}
            
          >
            <img src={Cart} alt="cart" width="25px" height="25px"></img>
          </span>
        ) : null}
        {!props.isLoggedin ? (
          <>
            <img
              src={User}
              alt="user"
              className="link"
              width="25px"
              height="25px"
              onClick={() => {
                setUserDropdown(prevState => !prevState);
                setCartDropdown(false);
              }}
              style={{ paddingLeft: "3.8em" }}
            ></img>
          </>
        ) : !props.isAdmin && !props.isSellers ? (
          <i
            className="far fa-user-circle link"
            style={{ fontSize: "1.5em", paddingLeft: '2.6em' }}
            onClick={() => {
              setShowAccounts(prevState => !prevState);
              setCartDropdown(false);
            }}
          ></i>
        ) : (
          <NavLink to="/" onClick={() => props.logout()}>
            LOGOUT
          </NavLink>
        )}
      </div>
      {/* CART DROPDOWN */}
      <div className={`${cartDropdown ? `cart` : `hiddenDropdown`}`}>
        {cartDropdown ? (
          <>
            <h1>
              My Cart.{" "}
              <span role="img" aria-label="face">
                😎
              </span>
            </h1>
            {props.cart.length === 0 ? (
              <div>
                <p>Your cart is currently empty.</p>
                <NavLink
                  className="yellowbutton"
                  onClick={() => setCartDropdown(false)}
                  to="/menu"
                >
                  GO TO CHALLENGES NOW
                </NavLink>
              </div>
            ) : (
              <>
                {props.cart.map(el => (
                  <div className="CartItem smallCart" key={el.id}>
                    <img
                      src={el.image}
                      alt="product"
                      width="60px"
                      height="60px"
                    />
                    <div style={{ display: "flex" }}>
                      <p>{el.quantity} x</p>
                      <p style={{ paddingLeft: "0.5em", width: "80%" }}>
                        {el.name}
                      </p>
                    </div>
                    <p>{el.price} €</p>
                  </div>
                ))}
                <div style={{ textAlign: "right" }}>
                  <h3>Total: {total} €</h3>
                  <NavLink
                    onClick={() => setCartDropdown(false)}
                    to="/checkout"
                    className="yellowbutton"
                  >
                    Checkout <i className="fas fa-arrow-right"></i>
                  </NavLink>
                </div>
              </>
            )}
          </>
        ) : null}
      </div>
      {/* USER DROPDOWN */}
      <div className={`${userDropdown ? `userdropdown` : `hiddenDropdown`}`}>
        {userDropdown ? (
          <>
            <NavLink to="/register/user" onClick={() => setUserDropdown(false)}>
              <span role="img" aria-label="pizza">
                 🏄🏼‍♀️
              </span>{" "}
              I'M A BUYER
            </NavLink>
            <NavLink to="/register/seller" onClick={() => setUserDropdown(false)}>
              <span role="img" aria-label="sellers">
                👩🏻‍🏫
              </span>{" "}
              I'M A SELLER
            </NavLink>
            <NavLink
              to="/register/admin"
              onClick={() => setUserDropdown(false)}
            >
              <span role="img" aria-label="cool">
                😎
              </span>{" "}
              I'M AN ADMIN
            </NavLink>
          </>
        ) : null}
      </div>
      {/* ACCOUNT DROPDOWN */}
      {showAccounts ? (
        <div className={`${showAccounts ? `account` : `hiddenDropdown`}`}>
          <NavLink
            to={`/dashboard/${props.username}`}
            onClick={() => setShowAccounts(false)}
          >
            YOUR ACCOUNT
          </NavLink>
          <NavLink
            to="/"
            onClick={() => {
              props.logout();
              setShowAccounts(false);
            }}
          >
            LOGOUT
          </NavLink>
        </div>
      ) : null}
    </div>
  );
};

export default Navbar;
