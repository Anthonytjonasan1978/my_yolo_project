import React, {useState, useEffect} from "react";
import { NavLink } from "react-router-dom";
import Loader from '../Loader/Loader';

const AccessError = props => {
    const [finishRender, setFinishRender] = useState(false);

    useEffect(() => {
        const timer = setTimeout(() => {
          setFinishRender(true);
        }, 1000);
        return () => clearTimeout(timer);
      }, []);

      let page;

      if (!finishRender) {
          page = <Loader />
      } else {
          page = 
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh'}}>
          <div>
          {props.isLoggedin ? (
            <h1>
              I'm sorry, this page is only accessible if you are
              {props.state === "admin" ? " an admin" :props.state === 'seller' ? " a seller": 'a registered user'}
            </h1>
          ) : (
            <div style={{ textAlign: 'center'}}>
              <h1>
                You must be logged in as
                {props.state === "admin" ? " an admin." : " a seller."}
              </h1>
    
              <NavLink
              className="yellowbutton"
                to={`/register/${props.state === "admin" ? `admin` : `seller`}`}
              >
                LOG IN AS {props.state === "admin" ? `AN ADMIN` : `A Seller`}
              </NavLink>
            </div>
          )}
        </div>
        </div>
      }
  return (
    <>{page}</>
  );
};

export default AccessError;
