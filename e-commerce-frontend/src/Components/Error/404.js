import React from 'react';
import {NavLink} from 'react-router-dom';
import Error from '../../Assets/404.svg';

import './Error.css';

const error = props => {
    return (
        <div className="ErrorContainer">
            <div>
            <img src={Error} alt="error"/>
            </div>
            <h1>Page can't be found</h1>
            <NavLink className="yellowbutton" to="/">GO TO HOME</NavLink>
        </div>
    )
}

export default error;